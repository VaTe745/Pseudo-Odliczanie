#include <iostream>
#include <Windows.h>

using namespace std;

int main() {
	setlocale(LC_ALL, "");

	cout << "Podaj liczbę (w sekundach) od której ma rozpocząć się odliczanie: \n \n";
	int liczba;
	cin >> liczba;
	if (liczba < 1) {
		cout << "\nWTF?! \n";
		return 0;
	}
	for (size_t i = 0; i < liczba; i++) {
		cout << endl << "Odliczanie: " << liczba - i << ". \n";
		Sleep(1000);
		if (liczba - i < 2)
			cout << endl << "Odliczanie dobiegło końca. \n";
	}
	return 0;
}
